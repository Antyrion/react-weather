import React, { Component } from 'react';
import Form from './components/form';
import Weather from './components/weather';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {createStore} from 'redux';

function reducer() {
  if (action.type === 'changeState')
    {
      console.log(action);
      return action.payload.newState;
    }
    return 'State';
}

const store = createStore(reducer);

console.log(store.getState());

const action = {
  type: 'changeState',
  payload: {
    newState: 'New State'
  }
};

store.dispatch(action);

const API_KEY = "3d2a7d4fe123586934759b269641673b";


class App extends Component {

  state = {
    city: undefined,
    temperature: undefined,
    wind_speed: undefined,
    humidity: undefined,
    pressure: undefined

  }

  getWeather = async (e) => {

    e.preventDefault();
    const city = e.target.elements.city.value;
    const api_call = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}&units=metric`);
    const data = await api_call.json();
    console.log(data);
    console.log(data.main.humidity);
    this.setState({
      temperature: data.main.temp,
      city: data.name,
      humidity: data.main.humidity,
      pressure: data.main.pressure
    })
  }
  render() {


    return (
      <div className="App">
        <h1> Weather Cast for today:</h1>
        <Form getWeather={this.getWeather} />
        <Weather temperature={this.state.temperature}
        city={this.state.city}
        humidity={this.state.humidity}
        pressure={this.state.pressure}
        />
      </div>
    );
  }
}

export default App;

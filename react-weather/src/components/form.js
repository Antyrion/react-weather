import React from 'react';
var gowno = {
    width: '400px',
    margin: '0 auto'
};
class Form extends React.Component{

    render(){

        return(
                <form onSubmit={this.props.getWeather}>
                    <input class="form-control" style={gowno} type='text' name='city' placeholder="city"></input>
                    <button className="btn btn-primary" type="submit">Send</button>
                </form>
            
        )
    }

}

export default Form;
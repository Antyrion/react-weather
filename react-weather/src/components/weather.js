import React from 'react'

class Weather extends React.Component{
render(){
    return(
        <div>
            <div >City: {this.props.city}</div>
           <div>Humidity: {this.props.humidity}</div>
           <div>
               Temperature: {this.props.temperature}
           </div>
           <div>
               Pressure: {this.props.pressure}
           </div>
        </div>
    );
}
}

export default Weather